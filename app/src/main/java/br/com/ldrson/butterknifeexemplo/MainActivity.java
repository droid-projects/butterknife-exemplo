package br.com.ldrson.butterknifeexemplo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textView) TextView mTextView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.button) Button mButton;

    @OnClick(R.id.button)
    public void buttonClick() {
        Toast.makeText(this,"Click do botão",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setupToolbar();

        mTextView.setText(getString(R.string.novo_texto));
    }

    private void setupToolbar(){
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
    }

}
